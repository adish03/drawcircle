//
//  UIView+Ext.swift
//  DrawCircle
//
//  Created by Bajzakov Adilet on 24/4/23.
//

import Foundation
import UIKit
import SnapKit

extension UIView {
    var safeArea: ConstraintBasicAttributesDSL {
#if swift(>=3.2)
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp
        }
        return self.snp
#else
        return self.snp
#endif
    }
    
    func width(divider: Double) -> Double {
        return frame.width / divider
    }
    
    func height(divider: Double) -> Double {
        return frame.height / divider
    }
}
