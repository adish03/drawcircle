//
//  DrawningView.swift
//  DrawCircle
//
//  Created by Bajzakov Adilet on 22/4/23.
//

import UIKit
import SnapKit

class DrawingView: UIView {
    
    private var path: UIBezierPath?
    private var lastPoint: CGPoint?
    private var timer: Timer?
    
    var quality: Double = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let lineWidth = CGFloat(5.0)
        let circleRect = bounds.insetBy(dx: lineWidth/2, dy: lineWidth/2)
        let circlePath = UIBezierPath(ovalIn: circleRect)
        UIColor.red.setStroke()
        circlePath.lineWidth = lineWidth
        circlePath.stroke()
        
        path?.stroke()
        
        let percentage = CGFloat(max(min(quality, 1.0), 0.0))
        let color = UIColor(hue: percentage * 0.3, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        color.setStroke()
        path?.lineWidth = lineWidth
    }
    
    func startDrawing(at point: CGPoint) {
        path = UIBezierPath()
        path?.move(to: point)
        lastPoint = point
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { [weak self] _ in
            self?.timerFired()
        }
    }
    
    func updateDrawing(to point: CGPoint) {}
    func endDrawing() {}
    func resetDrawing() {}
    private func timerFired() {
        resetDrawing()
    }
}
