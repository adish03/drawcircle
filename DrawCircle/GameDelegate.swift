//
//  GameDelegate.swift
//  DrawCircle
//
//  Created by Bajzakov Adilet on 22/4/23.
//

import Foundation

enum GameResult {
    case success
    case tooClose
    case failure
    
    var title: String {
        switch self {
        case .success:
            return "Поздравляем!"
        case .tooClose:
            return "Очень близко!"
        case .failure:
            return "Попробуйте снова"
        }
    }
    var message: String {
        switch self {
        case .success:
            return "Вы нарисовали идеальный круг!"
        case .tooClose:
            return "Слишком близко к центру!"
        case .failure:
            return "Это не круг!"
        }
    }
}

protocol GameDelegate: AnyObject {
    func gameDidUpdateQuality(_ quality: Double)
    func gameDidFinish(withResult result: GameResult)
}

class Game {
    private weak var delegate: GameDelegate?
    private var drawingPoints = [CGPoint]()
    private var quality = 0.0
    private let minPoints = 20
    
    init(delegate: GameDelegate) {
        self.delegate = delegate
    }
    func startDrawing(at point: CGPoint) {
        drawingPoints.append(point)
    }
    func updateDrawing(at point: CGPoint) {
        drawingPoints.append(point)
        calculateQuality()
    }
    func endDrawing() {
        calculateQuality()
    }
    func restart() {
        drawingPoints.removeAll()
        quality = 0
    }
    private func calculateQuality() {
        guard drawingPoints.count > minPoints else {
            delegate?.gameDidUpdateQuality(0)
            return
        }
    }
}
