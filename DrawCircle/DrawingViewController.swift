//
//  ViewController.swift
//  DrawCircle
//
//  Created by Bajzakov Adilet on 22/4/23.
//

import UIKit
import SnapKit

class DrawingViewController: UIViewController {

    private var drawingView: DrawingView!
    private var game: Game!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDrawingView()
        setupGame()
    }
    private func setupDrawingView() {
        drawingView = DrawingView()
        view.addSubview(drawingView)
        drawingView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    private func setupGame() {
        game = Game(delegate: self)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: drawingView)
        game.startDrawing(at: location)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: drawingView)
        game.updateDrawing(at: location)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        game.endDrawing()
    }
}
extension DrawingViewController: GameDelegate {
    
    func gameDidUpdateQuality(_ quality: Double) {
        drawingView.quality = quality
    }
    func gameDidFinish(withResult result: GameResult) {
        let alertController = UIAlertController(title: result.title,
                                                message: result.message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Начать заново",
                                                style: .default,
                                                handler: { [weak self] _ in
            self?.game.restart()
        }))
        present(alertController, animated: true, completion: nil)
    }
}
