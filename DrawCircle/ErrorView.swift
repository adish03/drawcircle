//
//  ErrorView.swift
//  DrawCircle
//
//  Created by Bajzakov Adilet on 24/4/23.
//

import Foundation
import UIKit
import SnapKit

class ErrorView: UIView {
    private let errorLabel = UILabel()
    private var hideTimer: Timer?

    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor(white: 0, alpha: 0.5)
        layer.cornerRadius = 10
        clipsToBounds = true

        errorLabel.textColor = .white
        errorLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        errorLabel.textAlignment = .center
        addSubview(errorLabel)

        errorLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(10)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showErrorMessage(_ message: String) {
        errorLabel.text = message
        alpha = 1
        hideTimer?.invalidate()
        hideTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { [weak self] _ in
            self?.hide()
        })
    }

    func hide() {
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
}
